﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class MessageClient
	{
		public StreamReader Reader { get; set; }
		public StreamWriter Writer { get; set; }

		public MessageWrapper Send(IMessage message)
		{
			//Console.WriteLine(message.ToJson());

			Writer.WriteLine(message.ToJson());
			string line;
			if ((line = Reader.ReadLine()) != null)
			{
				//Console.WriteLine(line);
				return JsonConvert.DeserializeObject<MessageWrapper>(line);
			}

			return null;
		}

	}
}
