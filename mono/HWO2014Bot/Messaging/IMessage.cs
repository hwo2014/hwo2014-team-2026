﻿using System;

namespace HWO2014Bot.Messaging
{
	public interface IMessage
	{
		Object MessageData();
		string MessageType();

		string ToJson();
	}
}
