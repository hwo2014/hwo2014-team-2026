﻿using HWO2014Bot.Model;
using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class CreateRaceMessage : MessageBase<CreateRaceMessage>
	{
		[JsonProperty(PropertyName = "botId")]
		public BotId BotId { get; set; }

		[JsonProperty(PropertyName = "trackName")]
		public string TrackName { get; set; }

		[JsonProperty(PropertyName = "password")]
		public string Password { get; set; }

		[JsonProperty(PropertyName = "carCount")]
		public int CarCount { get; set; }

		public override string MessageType()
		{
			return "createRace";
		}
	}
}
