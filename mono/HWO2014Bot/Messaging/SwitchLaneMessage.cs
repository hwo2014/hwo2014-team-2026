﻿using System;
using HWO2014Bot.Model;
using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class SwitchLaneMessage : MessageBase<SwitchLaneMessage>
	{
		[JsonIgnore]
		public TrackDirection Direction { get; set; }

		public override string MessageType()
		{
			return "switchLane";
		}

		public override object MessageData()
		{
			return Enum.GetName(typeof(TrackDirection), Direction);
		}
	}
}
