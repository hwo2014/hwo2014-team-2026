﻿
namespace HWO2014Bot.Messaging
{
	public class TurboMessage : MessageBase<TurboMessage>
	{
		public override string MessageType()
		{
			return "turbo";
		}

		public override object MessageData()
		{
			return "Prepare for ludicrous speed!";
		}
	}
}
