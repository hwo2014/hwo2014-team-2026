﻿using System.Collections.Generic;
using HWO2014Bot.Model;
using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class GameEndMessage : MessageBase<GameEndMessage>
	{
		[JsonProperty(PropertyName = "results")]
		public IEnumerable<Result> Results { get; set; }

		[JsonProperty(PropertyName = "bestLaps")]
		public IEnumerable<Result> BestLaps { get; set; }
	}
}
