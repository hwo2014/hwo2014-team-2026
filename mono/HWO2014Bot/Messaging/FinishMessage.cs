﻿using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class FinishMessage : MessageBase<FinishMessage>
	{
		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "color")]
		public string Color { get; set; }
	}
}
