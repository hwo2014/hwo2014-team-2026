﻿using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class SpawnMessage : MessageBase<SpawnMessage>
	{
		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "color")]
		public string Color { get; set; }
	}
}
