﻿using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class MessageWrapper
	{
		[JsonProperty(PropertyName = "msgType")]
		public string MessageType { get; set; }

		[JsonProperty(PropertyName = "data")]
		public object Data { get; set; }

		[JsonProperty(PropertyName = "gameId")]
		public string GameId { get; set; }

		[JsonProperty(PropertyName = "gameTick")]
		public int GameTick { get; set; }
	}
}
