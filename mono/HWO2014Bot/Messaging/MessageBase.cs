﻿using System;
using HWO2014Bot.Ai;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HWO2014Bot.Messaging
{
	public class MessageBase<T> : IMessage where T : IMessage, new()
	{
		public virtual Object MessageData()
		{
			return this;
		}

		public virtual string MessageType()
		{
			return string.Empty;
		}

		public string ToJson()
		{
			return JsonConvert.SerializeObject(
				new MessageWrapper {
					MessageType = MessageType(), 
					Data = MessageData(),
					GameTick = GameState.GameTick,
					GameId = GameState.GameId
				});
		}

		public static T MessageFromJson(string json)
		{
			return MessageFromData(GetDataObject(json));
		}

		public static T MessageFromData(object data)
		{
			return data != null ? JsonConvert.DeserializeObject<T>(data.ToString()) : new T() ;
		}

		public static JToken GetDataObject(string json)
		{
			return JObject.Parse(json)["data"];
		}
	}
}
