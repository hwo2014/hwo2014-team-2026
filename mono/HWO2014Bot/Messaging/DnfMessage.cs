﻿using HWO2014Bot.Model;
using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class DnfMessage : MessageBase<DnfMessage>
	{
		[JsonProperty(PropertyName = "car")]
		public CarId CarId { get; set; }
	}
}
