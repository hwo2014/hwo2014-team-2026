﻿using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class JoinMessage : MessageBase<JoinMessage>
	{
		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "key")]
		public string Key { get; set; }

		public override string MessageType()
		{
			return "join";
		}
	}
}
