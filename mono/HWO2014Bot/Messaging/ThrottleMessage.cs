﻿using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class ThrottleMessage : MessageBase<ThrottleMessage>
	{
		[JsonIgnore]
		public double Throttle { get; set; }

		public override string MessageType()
		{
			return "throttle";
		}

		public override object MessageData()
		{
			return Throttle;
		}
	}
}
