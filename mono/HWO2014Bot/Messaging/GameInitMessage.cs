﻿using HWO2014Bot.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HWO2014Bot.Messaging
{
	public class GameInitMessage : MessageBase<GameInitMessage>
	{
		[JsonProperty(PropertyName = "race")]
		public Race Race { get; set; }

		public static new GameInitMessage MessageFromWrapper(MessageWrapper wrapper)
		{
			var race = JsonConvert.DeserializeObject<Race>(((JObject)wrapper.Data)["race"].ToString());
			return new GameInitMessage { Race = race };
		}
	}
}
