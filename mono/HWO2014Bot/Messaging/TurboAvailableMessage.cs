﻿using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class TurboAvailableMessage : MessageBase<TurboAvailableMessage>
	{
		[JsonProperty(PropertyName = "turboDurationMilliseconds")]
		public double TurboDurationMilliseconds { get; set; }

		[JsonProperty(PropertyName = "turboDurationTicks")]
		public int TurboDurationTicks { get; set; }

		[JsonProperty(PropertyName = "turboFactor")]
		public double TurboFactor { get; set; }
	}
}
