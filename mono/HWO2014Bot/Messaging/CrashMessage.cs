﻿using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class CrashMessage : MessageBase<CrashMessage>
	{
		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "color")]
		public string Color { get; set; }
	}
}
