﻿
namespace HWO2014Bot.Messaging
{
	public class PingMessage : MessageBase<PingMessage>
	{
		public override string MessageType()
		{
			return "ping";
		}
	}
}
