﻿using HWO2014Bot.Model;
using Newtonsoft.Json;

namespace HWO2014Bot.Messaging
{
	public class LapFinishedMessage : MessageBase<LapFinishedMessage>
	{
		[JsonProperty(PropertyName = "car")]
		public CarId CarId { get; set; }

		[JsonProperty(PropertyName = "lapTime")]
		public Timing LapTime { get; set; }

		[JsonProperty(PropertyName = "raceTime")]
		public Timing RaceTime { get; set; }

	}
}
