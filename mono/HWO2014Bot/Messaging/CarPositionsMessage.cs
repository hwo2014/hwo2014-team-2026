﻿using System.Collections.Generic;
using System.Linq;
using HWO2014Bot.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HWO2014Bot.Messaging
{
	public class CarPositionsMessage : MessageBase<CarPositionsMessage>
	{
		[JsonIgnore]
		public IEnumerable<Car> Cars { get; set; }

		[JsonIgnore]
		public string GameId { get; set; }

		[JsonIgnore]
		public int GameTick { get; set; }

		public static new CarPositionsMessage MessageFromWrapper(MessageWrapper wrapper)
		{
			var cars = ((JArray)wrapper.Data).Select(c => JsonConvert.DeserializeObject<Car>(c.ToString()));
			var gameId = wrapper.GameId;
			var gameTick = wrapper.GameTick;

			return new CarPositionsMessage
			{
				Cars = cars,
				GameId = gameId,
				GameTick = gameTick
			};
		}
	}
}
