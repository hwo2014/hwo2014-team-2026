﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using HWO2014Bot.Ai;
using HWO2014Bot.Model;

namespace HWO2014Bot.Util
{
	public static class CarUtil
	{
		public static double Speed(this Car car)
		{
			if (car.RaceLogs == null || car.RaceLogs.Count <= 1)
			{
				return 0d;
			}

			var track = GameState.CurrentTrack();
			var endPiece = car.LastLog().TrackPiece;
			var startPiece = car.LastLog().Previous().TrackPiece;
			var endPosition = car.LastLog().InPieceDistance;
			var startPosition = car.LastLog().Previous().InPieceDistance;
			var laneIndex = track.InsideLane(startPiece);

			var speed = track.Distance(startPiece, endPiece, laneIndex, startPosition, endPosition);

			return speed < 12
				? speed
				: car.LastLog().Speed;
		}

		public static double Speed(this Car car, int gameTick)
		{
			var track = GameState.CurrentTrack();

			if (
				gameTick < 0 ||
				gameTick >= GameState.GameTick - 1 ||
				car.RaceLogs == null || 
				car.RaceLogs.Count == 0 || 
				car.RaceLogs.All(l => l.Tick != gameTick))
			{
				return car.Speed();
			}

			var start = car.RaceLogs.First(l => l.Tick == gameTick);

			if (start.Tick == car.RaceLogs.Last().Tick)
			{
				return car.Speed();
			}

			var end = car.RaceLogs.First(l => l.Tick == gameTick + 1);

			return track.Distance(start.TrackPiece, end.TrackPiece, end.EndLane, start.InPieceDistance, end.InPieceDistance);
		}

		public static double BrakingDistance(this Car car, double targetSpeed, double deceleration, double currentSpeed = -1)
		{
			var speed = currentSpeed > -1 
				? currentSpeed
				: car.LastLog().Speed;
			if (speed > targetSpeed)
			{
				return MathUtil.DistanceToTargetSpeed(speed, targetSpeed, -deceleration);
			}

			return 0.0d;
		}

		public static bool IsInLane(this Car car, int laneIndex)
		{
			return car.Position.Lane.EndLaneIndex == laneIndex;
		}

		public static bool IsAheadOf(this Car car, Car otherCar, bool ignoreLap = true)
		{
			if (!ignoreLap && car.Lap > otherCar.Lap)
			{
				return true;
			}

			if (car.Position.PieceIndex == otherCar.Position.PieceIndex)
			{
				return car.Position.InPieceDistance > otherCar.Position.InPieceDistance;
			}

			return car.Position.PieceIndex > otherCar.Position.PieceIndex;
		}

		public static double DistanceTo(this Car car, Car otherCar)
		{
			if (car.IsAheadOf(otherCar))
			{
				return -GameState.CurrentTrack().Distance(
					otherCar.Position.PieceIndex,
					car.Position.PieceIndex,
					otherCar.Position.Lane.EndLaneIndex,
					otherCar.Position.InPieceDistance,
					car.Position.InPieceDistance
				);
			}

			return GameState.CurrentTrack().Distance(
					car.Position.PieceIndex,
					otherCar.Position.PieceIndex,
					car.Position.Lane.EndLaneIndex,
					car.Position.InPieceDistance,
					otherCar.Position.InPieceDistance
				);
		}

		public static Car CarInFront(this Car car, int lane = -1)
		{
			if (lane == -1)
			{
				lane = car.Position.Lane.EndLaneIndex;
			}

			return
				GameState.Cars.Values.Where(c => c.IsAheadOf(car) && c.IsInLane(lane))
					.OrderBy(car.DistanceTo)
					.FirstOrDefault();
		}

		public static RaceLog LastLog(this Car car)
		{
			if (car.RaceLogs == null || car.RaceLogs.Count == 0)
			{
				return null;
			}
			return car.RaceLogs.Last();
		}

		public static List<RaceLog> LastLogs(this Car car, int amount)
		{
			return car.RaceLogs.GetRange(car.RaceLogs.IndexOf(car.LastLog()) - amount, amount);
		}

		public static RaceLog Previous(this RaceLog raceLog)
		{
			var tick = raceLog.Tick - 1;

			if (tick < 0)
			{
				tick = 0;
			}

			return raceLog.Car.RaceLogs.FirstOrDefault(l => l.Tick == tick);
		}

		public static List<RaceLog> LogsBetween(this Car car, int start, int end)
		{
			return car.RaceLogs.Where(l => l.Tick.IsBetween(start, end)).OrderBy(l => l.Tick).ToList();
		}

		public static double AverageSpeed(this Car car, int start, int end)
		{
			return car.LogsBetween(start, end).Average(l => l.Speed);
		}

		public static double AverageAcceleration(this Car car, int start, int end)
		{
			var logEnum = car.LogsBetween(start, end).GetEnumerator();
			double speed = -1;
			var accelerations = new List<double>();

			while (logEnum.MoveNext())
			{
				if (logEnum.Current == null) continue;
				if (speed > -1)
				{
					accelerations.Add(logEnum.Current.Speed - speed);
				}

				speed = logEnum.Current.Speed;
			}

			return accelerations.Average();
		}

		public static double RelativeSpeed(this Car car, Car otherCar)
		{
			return otherCar.LastLogs(3).Average(l => l.Speed) - otherCar.LastLogs(3).Average(l => l.Speed);
		}

		public static bool IsFaster(this Car car, Car otherCar)
		{
			return car.RelativeSpeed(otherCar) < 0;
		}

		public static double CentrifugalForce(this Car car)
		{
			return MathUtil.CalculateCentrifugalForce(
				car.Speed(), 
				GameState.CurrentTrack().PieceAt(car.Position.PieceIndex).Radius);
		}

		public static int CurrentLane(this Car car)
		{
			return car.Position.Lane.EndLaneIndex;
		}

		public static TrackDirection GetSwitchDirection(this Car car, int targetLane)
		{
			if (car.CurrentLane() < targetLane)
			{
				return TrackDirection.Right;
			}

			if (car.CurrentLane() > targetLane)
			{
				return TrackDirection.Left;
			}

			return TrackDirection.None;
		}

		public static TrackDirection FindFreeLane(this Car car, double distance)
		{
			var carInFront = car.CarInFront();

			if (carInFront == null || car.DistanceTo(carInFront) > distance)
			{
				return TrackDirection.None;
			}

			if (car.CarInFront(GameState.CurrentTrack().InnerLane()) == null)
			{
				return car.GetSwitchDirection(GameState.CurrentTrack().InnerLane());
			}

			if (car.CarInFront(GameState.CurrentTrack().OuterLane()) == null)
			{
				return car.GetSwitchDirection(GameState.CurrentTrack().OuterLane());
			}

			return TrackDirection.None;
		}

		public static double AngleChange(this Car car, int time = -1)
		{
			var tick = time == -1
				? GameState.GameTick
				: time;

			var log1 = car.RaceLogs.FirstOrDefault(l => l.Tick == tick);
			var log2 = car.RaceLogs.FirstOrDefault(l => l.Tick == tick - 1);

			if (log1 == null || log2 == null)
			{
				return 0;
			}

			return log1.Angle - log2.Angle;
		}
	}
}
