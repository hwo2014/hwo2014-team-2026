﻿using System;
using System.Collections.Generic;
using System.Linq;
using HWO2014Bot.Model;

namespace HWO2014Bot.Util
{
	public static class TrackPieceUtil
	{
		public static bool IsStraight(this TrackPiece piece)
		{
			return piece.Length > 0;
		}

		public static bool IsCurve(this TrackPiece piece)
		{
			return piece.Radius > 0;
		}

		public static bool IsSwitch(this TrackPiece piece)
		{
			return piece.Switch;
		}

		public static TrackDirection CurveDirection(this TrackPiece piece)
		{
			if (piece.IsStraight())
			{
				return TrackDirection.None;
			}

			return piece.Angle > 0 ? TrackDirection.Right : TrackDirection.Left;
		}

		public static TrackPiece NextOnTrack(this TrackPiece piece)
		{

			return piece.Track.PieceAt(piece.Index + 1);
		}

		public static TrackPiece PreviousOnTrack(this TrackPiece piece)
		{
			return piece.Track.PieceAt(piece.Index - 1);
		}

		public static double PieceLength(this TrackPiece piece, int laneIndex = -1)
		{
			if (piece.IsStraight())
			{
				return piece.Length;
			}

			if (piece.Radius == 0)
			{
				return 0d;
			}

			laneIndex = laneIndex == -1
				? piece.Track.InsideLane(piece.Index)
				: laneIndex;


			var lane = piece.Track.Lanes.First(l => l.Index == laneIndex);
			double laneRadius = piece.Radius - lane.DistanceFromCenter;
			return (Math.PI * Math.Abs(piece.Angle) / 180.0d) * laneRadius;
		}

		public static double SectionLength(this IEnumerable<TrackPiece> pieces)
		{
			return pieces.Sum(p => p.PieceLength(p.Track.InnerLane()));
		}
	}
}
