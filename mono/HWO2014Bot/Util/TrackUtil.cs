﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using HWO2014Bot.Ai;
using HWO2014Bot.Model;

namespace HWO2014Bot.Util
{
	public static class TrackUtil
	{
		public static int CurrentPiece()
		{
			var position = GameState.BotCar().Position;
			return position != null ? position.PieceIndex : 0;
		}

		public static double CurrentPieceDistance()
		{
			var position = GameState.BotCar().Position;
			return position != null ? position.InPieceDistance : 0d;
		}

		public static TrackPiece CurrentPiece(this Track track)
		{
			return track.PieceAt(CurrentPiece());
		}

		public static TrackPiece NextPiece(this Track track)
		{
			return track.NextPiece(CurrentPiece());
		}

		public static TrackPiece NextPiece(this Track track, int index)
		{
			var position = index + 1;
			return track.PieceAt(position);
		}

		public static TrackPiece PieceAt(this Track track, int index)
		{
			if (index < 0)
			{
				return track.Pieces.Last();
			}

			return index < track.Pieces.Count
				? track.Pieces[index]
				: track.Pieces.First();
		}

		public static int StartOfNextCurve(this Track track, int startIndex = -1)
		{

			var index = startIndex == -1
				? CurrentPiece()
				: startIndex;

			var piece = track.PieceAt(index);

			while (piece.IsStraight() || piece.CurveDirection() == piece.PreviousOnTrack().CurveDirection())
			{
				piece = piece.NextOnTrack();

				if (piece.Index == index)
				{
					break;
				}
			}

			return piece.Index;
		}

		public static int EndOfNextCurve(this Track track, int startIndex = -1)
		{
			var start = track.StartOfNextCurve(startIndex);

			if (start == -1)
			{
				return -1;
			}

			var index = start + 1;

			var breakAtEnd = false;

			while (track.PieceAt(index).IsCurve() && track.PieceAt(index).CurveDirection() == track.PieceAt(start).CurveDirection())
			{
				index++;

				if (index + 1 == start)
				{
					break;
				}

				if (index == track.Pieces.Count)
				{
					if (start == 0)
					{
						return index - 1;
					}

					index = 0;

					if (breakAtEnd)
					{
						return -1;
					}

					breakAtEnd = true;
				}
			}

			return index;
		}

		public static double DistanceToNextCurve(this Track track)
		{
			var position = GameState.BotCar().Position;
			return track.Distance(position.PieceIndex, track.StartOfNextCurve(), position.Lane.EndLaneIndex,
				position.InPieceDistance, 0);
		}

		public static double DistanceToNextApex(this Track track)
		{
			return
				track.Distance(
					CurrentPiece(),
					track.StartOfNextCurve(),
					track.InsideLane(track.StartOfNextCurve()),
					CurrentPieceDistance(),
					0d) + track.DistanceFromStartToApex(track.StartOfNextCurve());
		}

		public static List<TrackPiece> NextCurvePieces(this Track track)
		{
			return track.PiecesBetween(track.StartOfNextCurve(), track.EndOfNextCurve());
		}

		public static int NextSwitchPosition(this Track track, int startIndex = -1)
		{
			var index = startIndex > -1 
				? startIndex
				: CurrentPiece();

			var piece = track.PieceAt(index);
			while (!piece.IsSwitch())
			{
				piece = piece.NextOnTrack();

				if (piece.Index == index)
				{
					return -1;
				}
			}

			return piece.Index;
		}

		public static List<TrackPiece> PiecesBetween(this Track track, int start, int end, bool includeLast = false)
		{
			var pieces = new List<TrackPiece>();

			if (start < 0 || end < 0)
			{
				return pieces;
			}

			if (start >= track.Pieces.Count)
			{
				start = track.Pieces.Count - 1;
			}

			if (end >= track.Pieces.Count)
			{
				end = track.Pieces.Count - 1;
			}

			if (start == end)
			{
				pieces.Add(track.Pieces[start]);
				return pieces;
			}

			var index = start;
			var breakAtEnd = false;

			while (index != end)
			{
				pieces.Add(track.Pieces[index]);

				index++;

				if (index == track.Pieces.Count)
				{
					if (breakAtEnd)
					{
						break;
					}

					index = 0;
					breakAtEnd = true;
				}
			}

			if (includeLast)
			{
				pieces.Add(track.PieceAt(end));
			}

			return pieces;
		}

		public static double PieceLength(this Track track, int pieceIndex, int laneIndex = 0)
		{
			var piece = track.Pieces[pieceIndex];

			return piece.PieceLength(laneIndex);
		}

		public static double Distance(this Track track, int startIndex, int endIndex, int laneIndex = -1, double startPosition = -1, double endPosition = -1, bool onlyForward = false)
		{
			var startPiece = startIndex;
			var endPiece = endIndex;
			var directionFactor = 1;

			if (endIndex < startIndex && !onlyForward)
			{
				startPiece = endIndex;
				endPiece = startIndex;
				directionFactor = -1;
			}



			if (laneIndex < 0)
			{
				laneIndex = track.InsideLane(startPiece);
			}

			if (startIndex == endPiece)
			{
				if (startPosition < 0 || endPosition < 0)
				{
					return track.PieceAt(startPiece).PieceLength(laneIndex);
				}

				return endPosition - startPosition;
			}

			double distance;

			if (endPiece - startPiece > 1)
			{
				var pieces = track.PiecesBetween(startPiece, endPiece);
				distance = pieces.Sum(p => p.PieceLength(laneIndex));
			}
			else
			{
				distance = track.PieceAt(startPiece).PieceLength(laneIndex);
			}

			if (startPosition > -1)
			{
				distance -= startPosition;

				if (distance < 0)
				{
					distance = 0d;
				}
			}

			if (endPosition > -1)
			{
				distance += endPosition;
			}

			//if (distance > 20)
			//{
			//	Console.WriteLine("{0} {1} {2} {3} {4} {5} {6}",
			//		startIndex,
			//		endIndex,
			//		startPosition,
			//		endPosition,
			//		laneIndex,
			//		track.PieceAt(startIndex).PieceLength(laneIndex),
			//		distance);
			//}
			
			return distance * directionFactor;
		}

		public static int InnerLane(this Track track)
		{
			var laneIndex = GameState.BotCar().Position.Lane.EndLaneIndex +
							Math.Sign(track.PieceAt(track.StartOfNextCurve()).Angle);

			if (laneIndex < 0)
			{
				return 0;
			}

			if (laneIndex > track.Lanes.Count - 1)
			{
				return track.Lanes.Count - 1;
			}

			return laneIndex;
		}

		public static int OuterLane(this Track track)
		{
			var laneIndex = GameState.BotCar().Position.Lane.EndLaneIndex -
							Math.Sign(track.PieceAt(track.StartOfNextCurve()).Angle);

			if (laneIndex < 0)
			{
				return 0;
			}

			if (laneIndex > track.Lanes.Count - 1)
			{
				return track.Lanes.Count - 1;
			}

			return laneIndex;
		}

		public static int InsideLane(this Track track, int pieceIndex)
		{
			var angle = track.PieceAt(pieceIndex).IsCurve()
				? track.PieceAt(pieceIndex).Angle
				: track.PieceAt(track.StartOfNextCurve(pieceIndex)).Angle;

			return angle > 0
				? track.Lanes.Last().Index
				: track.Lanes.First().Index;
		}

		public static double CurveIntensity(this Track track, int startIndex)
		{
			var curve = track.PiecesBetween(track.StartOfNextCurve(startIndex), track.EndOfNextCurve(startIndex), true);
			return curve.Sum(p => Math.Abs(p.Angle)) / curve.Where(p => p.Radius > 0).Min(p => (double)p.Radius) / 0.9d;
		}

		public static double TargetCorneringSpeed(this Track track, int position, double baseSpeed)
		{
			var speed = baseSpeed - (track.CurveIntensity(position) * 1.6);
			return speed;
		}

		public static double CurveArc(this Track track, int locationIndex)
		{
			var curve = track.PiecesBetween(track.StartOfCurve(locationIndex), track.EndOfCurve(locationIndex), true);
			return curve.Sum(p => Math.Abs(p.Angle));
		}

		public static double DistanceThroughCurve(this Track track, int pieceIndex, double pieceDistance, int laneIndex = -1)
		{
			var lane = laneIndex == -1
				? track.InsideLane(pieceIndex)
				: laneIndex;
			return track.Distance(track.StartOfCurve(pieceIndex), pieceIndex, lane, 0d, pieceDistance);
		}

		public static double AdvancedThroughCurve(this Track track, int pieceIndex, double pieceDistance, int laneIndex = -1)
		{
			var curveLength = track.Distance(track.StartOfCurve(pieceIndex), track.EndOfCurve());
			return track.DistanceThroughCurve(pieceIndex, pieceDistance, laneIndex)/curveLength;

		}

		public static int ShortestLane(this Track track, int startIndex, int endIndex, double startPosition = 0, double endPosition = 0)
		{
			var distances = track.Lanes.Select(l => track.Distance(startIndex, endIndex, l.Index, startPosition, endPosition)).ToList();
			return distances.IndexOf(distances.Min());
		}

		public static int NextCurveRadius(this Track track)
		{
			return track.NextCurvePieces().Where(p => p.Radius > 0).Min(p => p.Radius);
		}

		public static int CurveRadius(this Track track, int pieceOnCurve = -1)
		{
			if (pieceOnCurve == -1) return track.NextCurveRadius();

			return track.PiecesBetween(track.StartOfCurve(pieceOnCurve), track.EndOfCurve(pieceOnCurve), true)
				.Where(p => p.Radius > 0).Min(p => p.Radius);
		}

		public static double DistanceFromStartToApex(this Track track, int curveStart)
		{
			var pieces = track.PiecesBetween(curveStart, track.EndOfCurve(curveStart), true);
			var sharpPieces = pieces.Where(p => p.Radius == pieces.Min(p2 => p2.Radius)).ToList();
			var index = sharpPieces[(int) Math.Round((double) (sharpPieces.Count()-0.5)/2, MidpointRounding.AwayFromZero)].Index;
			return
				track.Distance(curveStart, index, track.InsideLane(index), 0, track.PieceAt(index).PieceLength()/2);

			//return
			//	track.PiecesBetween(curveStart, track.EndOfCurve(curveStart), true)
			//		.Sum(p => p.PieceLength(track.InnerLane())) / 2;
		}

		public static int StartOfCurve(this Track track, int pieceIndex = -1)
		{
			var index = pieceIndex == -1
				? CurrentPiece()
				: pieceIndex;

			var piece = track.PieceAt(index);

			if (piece.IsStraight())
			{
				return track.StartOfNextCurve(pieceIndex);
			}

			while (piece.IsCurve())
			{
				piece = piece.PreviousOnTrack();

				if (piece.Index == index)
				{
					break;
				}
			}

			return piece.NextOnTrack().Index;
		}

		public static int EndOfCurve(this Track track, int pieceIndex = -1)
		{
			var index = pieceIndex == -1
				? CurrentPiece()
				: pieceIndex;

			var piece = track.PieceAt(index);

			if (piece.IsStraight())
			{
				return track.EndOfNextCurve(pieceIndex);
			}

			while (piece.IsCurve())
			{
				piece = piece.NextOnTrack();
			}

			return piece.PreviousOnTrack().Index;
		}

		public static double DistanceToApex(this Track track, int curveStart, int pieceIndex, double pieceDistance)
		{
			return track.Distance(pieceIndex, curveStart, track.InsideLane(curveStart), pieceDistance, 0d) +
					track.DistanceFromStartToApex(curveStart);
		}

		public static double DistanceToNextSharpApex(this Track track, int pieceIndex = -1, double pieceDistance = 0 )
		{
			var index = pieceIndex == -1
				? track.CurrentPiece().Index
				: pieceIndex;

			var curveStart = track.StartOfNextCurve(index);
			var endSearch = curveStart;
			while (track.CurveArc(curveStart) < 61)
			{
				curveStart = track.StartOfNextCurve(track.EndOfCurve(curveStart));
				if (curveStart == endSearch) break;
			}

			return track.DistanceToApex(curveStart, index, pieceDistance);

		}
	}
}
 