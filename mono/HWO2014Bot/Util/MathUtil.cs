﻿using System;
using System.Collections.Generic;

namespace HWO2014Bot.Util
{
	public static class MathUtil
	{
		public const double BaseTolerance = 0.001d;

		public static bool IsZero(this double number)
		{
			return Math.Abs(number) < BaseTolerance;
		}

		public static double CalculateCentrifugalForce(double speed, int radius)
		{
			if (radius == 0)
			{
				return 0d;
			}

			return Math.Pow(speed, 2) / (double)radius;
		}

		public static double SpeedToCentrifugal(double acceleration, int radius)
		{
			return Math.Sqrt(acceleration * radius);
		}

		public static bool IsBetween<T>(this T item, T start, T end)
		{
			return Comparer<T>.Default.Compare(item, start) >= 0
				&& Comparer<T>.Default.Compare(item, end) <= 0;
		}

		public static double CalculateDistance(int ticks, double speed, double acceleration = 0.0d)
		{
			//s = vi t + 1/2 a t2
			return speed*ticks + 0.5*acceleration*Math.Pow(ticks, 2);
		}

		public static double CalculateSpeedAfterTime(int ticks, double speed, double acceleration)
		{
			//vf = vi + a t
			return speed + acceleration*ticks;
		}

		public static double CalculateSpeedAtDistance(double speed, double acceleration, double distance)
		{
			return speed + acceleration*(distance - 2d*acceleration/Math.Sqrt(distance))/speed;
		}

		public static double DistanceToTargetSpeed(double currentSpeed, double targetSpeed, double acceleration)
		{
			//var accPrefix = 1d;
			var accPrefix = currentSpeed > targetSpeed ? -1d : 1d;
			return (targetSpeed - currentSpeed)/(acceleration*accPrefix)*((currentSpeed + targetSpeed)/2d);
		}

		//public static double MaxSpeedBeforeBraking()
		//{
			
			
		//	(speed - targetSpeed)/deceleration*((speed + targetSpeed)/2)
		//}
	}
}
