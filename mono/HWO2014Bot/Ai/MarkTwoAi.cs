﻿using System;
using HWO2014Bot.Messaging;
using HWO2014Bot.Model;
using HWO2014Bot.Util;

namespace HWO2014Bot.Ai
{
	public class MarkTwoAi : AiHandlerBase
	{
		protected const double FullThrottle = 1.0d;
		protected const double NoThrottle = 0.0d;
		protected const double MinCurveThrottle = 0.5d;
		protected const double CurveThrottle = 0.7d;
		protected const double MinAngle = 20d;
		protected const double MaxAngle = 45d;
		protected const double CentrifugalBase = 0.0d;
		protected const double ThrottleStep = 0.1d;
		protected const double SpeedBump = 1.0d;
		

		private int NextSwitch { get; set; }

		protected double BaseAcceleration { get; set; }
		protected double BaseDeceleration { get; set; }
		protected bool RaceStarted { get; set; }
		protected bool Calibrated { get; set; }
		protected int CalibrationTick { get; set; }
		protected double OvertakingDistance { get; set; }

		protected double CornerSpeed { get; set; }
		protected double StraightSpeed { get; set; }
		public bool StraightSpeedSet { get; set; }

		protected Car BotCar
		{
			get { return GameState.BotCar(); }
		}

		protected Track CurrentTrack
		{
			get { return GameState.CurrentTrack(); }
		}

		protected double ThrottleFactor
		{
			get
			{
				if (TurboUsed)
				{
					return BaseAcceleration*GameState.TurboFactor;
				}

				return BaseAcceleration;
			}
		}

		protected bool TurboAvailable
		{
			get { return GameState.TurboAvailable > 0 && GameState.TurboUsed < GameState.TurboAvailable; }
		}

		protected bool TurboUsed
		{
			get { return GameState.TurboAvailable > 0 && GameState.TurboUsed > GameState.TurboAvailable && GameState.GameTick.IsBetween(GameState.TurboUsed, GameState.TurboUsed + GameState.TurboLength); }
		}


		public MarkTwoAi()
		{
			CalibrationTick = 5;
			NextSwitch = -1;
			OvertakingDistance = 100;
		}

		protected override IMessage CalculateMove()
		{
			LogState();

			if (!Calibrated)
			{
				return Throttle(Calibrate());
			}

			if (NextSwitch == -1)
			{
				NextSwitch = CurrentTrack.NextSwitchPosition();
			}

			// Switch - calculate optimal lane for next curve
			if (CurrentTrack.CurrentPiece().Index == NextSwitch || CurrentTrack.CurrentPiece().NextOnTrack().Index == NextSwitch)
			{
				var lane = BotCar.FindFreeLane(OvertakingDistance);

				if (lane != TrackDirection.None)
				{
					NextSwitch = CurrentTrack.NextSwitchPosition(CurrentTrack.PieceAt(NextSwitch).NextOnTrack().Index);
					return SwitchLane(lane);
				}

				//Console.WriteLine("Current lane {0}, shortest lane {1}", BotCar.Position.Lane.EndLaneIndex, shortestLaneNextCurve);
				
				lane = BotCar.GetSwitchDirection(CurrentTrack.ShortestLane(
					CurrentTrack.CurrentPiece().Index,
					CurrentTrack.NextSwitchPosition(CurrentTrack.CurrentPiece().NextOnTrack().Index)));
				//lane = BotCar.GetSwitchDirection(CurrentTrack.InnerLane());

				

				if (lane != TrackDirection.None)
				{
					NextSwitch = CurrentTrack.NextSwitchPosition(CurrentTrack.PieceAt(NextSwitch).NextOnTrack().Index);
					return SwitchLane(lane);
				}
			}

			var nextCorner = CurrentTrack.DistanceToNextSharpApex(BotCar.Position.PieceIndex, BotCar.Position.InPieceDistance);

			if ( StraightSpeed.IsZero() || (!StraightSpeedSet && CurrentTrack.CurrentPiece().IsCurve() && CurrentTrack.NextPiece().IsStraight()))
			{
				CornerSpeed = GetCornerSpeed();
				StraightSpeed = TargetSpeed(nextCorner, CornerSpeed);
				StraightSpeedSet = true;
			}

			double throttle;

			if (CurrentTrack.CurrentPiece().IsStraight())
			//if (BotCar.Angle < OptimumAngle)
			{
				if (CurrentTrack.CurrentPiece().NextOnTrack().NextOnTrack().IsStraight())
				{
					return Throttle(FullThrottle);
				}

				//var nextCorner = CurrentTrack.DistanceToNextSharpApex(BotCar.Position.PieceIndex, BotCar.Position.InPieceDistance);
				//CornerSpeed = GetCornerSpeed();
				//var targetSpeed = TargetSpeed(nextCorner, CornerSpeed);
				//if (BotCar.BrakingDistance(cornerSpeed, BaseDeceleration) < nextCorner)
				if (BotCar.Speed() < StraightSpeed)
				{
					if (TurboAvailable && CurrentTrack.NextPiece().IsStraight())
					{
						CornerSpeed = GetCornerSpeed();
						StraightSpeed = TargetSpeed(nextCorner, CornerSpeed);
						StraightSpeedSet = true;
						return Turbo();
					}

					throttle = FullThrottle;
				}
				else
				{
					StraightSpeedSet = false;
					throttle = BotCar.Speed() > CornerSpeed 
						? NoThrottle 
						: CurveThrottle;
					
					//throttle = FullThrottle / ThrottleFactor * (BaseAcceleration - BaseDeceleration);
					//throttle = (Math.Pow(targetSpeed, 2) - Math.Pow(BotCar.Speed(), 2))/(2*nextCorner)/BaseAcceleration;
				}
				//Console.WriteLine("Throttle {0}", throttle);
				
			}
			else
			{
				if (CurrentTrack.CurveArc(CurrentTrack.CurrentPiece().Index) > 100)
				{
					if (
						CurrentTrack.AdvancedThroughCurve(BotCar.Position.PieceIndex, BotCar.Position.InPieceDistance,
							BotCar.Position.Lane.EndLaneIndex) > 0.6 &&
						CurrentTrack.DistanceToApex(CurrentTrack.StartOfCurve(),BotCar.Position.PieceIndex, BotCar.Position.InPieceDistance) < 0)
					{
						throttle = BotCar.Angle < MaxAngle
							? GameState.LastThrottle + ThrottleStep
							: GameState.LastThrottle - ThrottleStep;
					}
					else
					{
						if (BotCar.Speed() > CornerSpeed)
						{
							throttle = NoThrottle;
						}
						else
						{
							throttle = CurveThrottle + ((CurrentTrack.CurrentPiece().Radius / 100d) - 1d)/2d;

							if (BotCar.AngleChange() < 0)
							{
								throttle += ThrottleStep;
							}
						}
						
					}
				}
				else
				{
					throttle = CurveThrottle;
				}

				if (throttle < MinCurveThrottle)
				{
					throttle = MinCurveThrottle;
				}

				//if (CurrentTrack.DistanceToApex(CurrentTrack.StartOfCurve(), BotCar.Position.PieceIndex,
				//	BotCar.Position.InPieceDistance) > 0)
				//{
				//	var nextCorner = CurrentTrack.DistanceToNextApex();
				//	CornerSpeed = GetCornerSpeed();
				//	throttle = BotCar.Angle < MaxAngle && BotCar.Speed() < TargetSpeed(nextCorner, CornerSpeed)
				//		? FullThrottle
				//		: GameState.LastThrottle - ThrottleStep;
				//}
				//else
				//{
				//	if (BotCar.Angle > MinAngle)
				//	{
				//		throttle = BotCar.AngleChange() > 0
				//			? GameState.LastThrottle - ThrottleStep
				//			: GameState.LastThrottle + ThrottleStep;
				//	}
				//	else
				//	{
				//		throttle = CurveThrottle;
				//	}
				//}


				//if (CornerSpeed > 0 && BotCar.Speed() > CornerSpeed)
				//{
				//	throttle = NoThrottle;
				//}
				//else
				//{
				//	throttle = (BotCar.CentrifugalForce() + CentrifugalBase) / ThrottleFactor;
				//}
			}

			if (BotCar.Speed().IsZero())
			{
				throttle = MinCurveThrottle;
			}

			if (throttle > FullThrottle)
			{
				throttle = FullThrottle;
			}

			if (throttle < NoThrottle)
			{
				throttle = NoThrottle;
			}

			return Throttle(throttle);
		}

		private void LogState()
		{
			Console.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}",
				GameState.CurrentLap,
				BotCar.LastLog().Tick,
				BotCar.LastLog().TrackPiece,
				BotCar.LastLog().InPieceDistance.ToString("N3"),
				BotCar.LastLog().Angle.ToString("N3"),
				BotCar.Speed().ToString("N3"),
				BotCar.CentrifugalForce().ToString("N3"),
				CurrentTrack.CurrentPiece().PieceLength(GameState.CurrentTrack().InnerLane()).ToString("N3"),
				CurrentTrack.CurrentPiece().Radius,
				CurrentTrack.CurrentPiece().Angle.ToString("N3"),
				GameState.LastThrottle.ToString("N3"),
				(GameState.TurboUsed + GameState.TurboLength > GameState.GameTick
					? GameState.LastThrottle*GameState.TurboFactor
					: GameState.LastThrottle).ToString("N3"),
				BotCar.LastLog().EndLane - BotCar.LastLog().StartLane
				);
		}

		private double GetCornerSpeed()
		{
			var radius = CurrentTrack.NextCurveRadius();
			return  MathUtil.SpeedToCentrifugal(BaseAcceleration + CentrifugalBase, radius);
		}

		private double TargetSpeed(double nextCorner, double cornerSpeed)
		{
			var t = 0;
			var targetSpeed = BotCar.Speed();

			while (t < 50)
			{
				t++;
				targetSpeed = MathUtil.CalculateSpeedAfterTime(t, targetSpeed, ThrottleFactor);

				if (targetSpeed < cornerSpeed)
				{
					continue;
				}

				var distance = MathUtil.DistanceToTargetSpeed(targetSpeed, cornerSpeed, BaseDeceleration);
				
				if (distance >= nextCorner)
				{
					if (t == 1)
					{
						targetSpeed = cornerSpeed;
					}
					break;
				}
			}

			targetSpeed += SpeedBump;
			//Console.WriteLine("Target speed {0}", targetSpeed);
			return targetSpeed;
		}

		private double Calibrate()
		{
			if (BaseAcceleration.IsZero())
			{
				if (GameState.GameTick < CalibrationTick || GameState.LastThrottle.IsZero())
				{
					return FullThrottle;
				}
				BaseAcceleration = BotCar.AverageAcceleration(0, CalibrationTick);
				Console.WriteLine("Base acceleration is  {0}", BaseAcceleration);
				return NoThrottle;
			}

			if (GameState.GameTick < CalibrationTick * 2)
			{
				return NoThrottle;
			}

			BaseDeceleration = -BotCar.AverageAcceleration(CalibrationTick + 1, GameState.GameTick);
			Console.WriteLine("Base deceleration is  {0}", BaseDeceleration);
			Calibrated = true;
			return FullThrottle;
		}
	}
}
