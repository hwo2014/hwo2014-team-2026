﻿using System.Collections.Generic;
using HWO2014Bot.Model;

namespace HWO2014Bot.Ai
{
	public static class GameState
	{
		public static Dictionary<string, Track> Tracks { get; set; }

		public static Dictionary<CarId, Car> Cars { get; set; }

		public static CarId BotCarId { get; set; }

		public static string CurrentTrackId { get; set; }

		public static int GameTick { get; set; }

		public static int CurrentLap { get; set; }

		public static string GameId { get; set; }

		public static Car BotCar()
		{
			return Cars[BotCarId];
		}

		public static Track CurrentTrack()
		{
			return Tracks[CurrentTrackId];
		}

		public static double LastThrottle { get; set; }

		public static TrackDirection LastLaneSwitch { get; set; }

		public static int TurboAvailable { get; set; }

		public static int TurboLength { get; set; }

		public static double TurboFactor { get; set; }

		public static int TurboUsed { get; set; }

		public static bool Crashed { get; set; }
	}
}
