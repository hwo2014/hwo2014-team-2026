﻿using System;
using System.Linq;
using HWO2014Bot.Messaging;
using HWO2014Bot.Model;
using HWO2014Bot.Util;

namespace HWO2014Bot.Ai
{
	public class MarkOneAi : AiHandlerBase
	{
		protected const double MaxAngle = 30.0;
		protected const double SlideAngle = 45.0;
		protected const double ThrottleDecrement = 0.15;
		protected const double CornerThrottle = 0.75;
		protected const double StraightThrottle = 1;
		protected const double MinThrottle = 0.45;
		protected const double SpeedBeforeCorner = 9;
		protected const double DefaultAcceleration = 0.2;
		protected const double DefaultDeceleration = 0.12;
		protected double BaseAcceleration = 0;
		protected double BaseDeceleration = 0;
		protected bool RaceStarted = false;
		protected bool Calibrated = false;
		protected int CalibrationTick = 10;
		protected const double BrakingDistanceMargin = 20;
		protected const double OvertakingDistance = 100;

		protected static Car BotCar
		{
			get { return GameState.BotCar(); }
		}

		protected static Track CurrentTrack
		{
			get { return GameState.CurrentTrack(); }
		}

		private bool allowSwitch = true; 

		protected override IMessage CalculateMove()
		{
			if (!Calibrated)
			{
				return Throttle(Calibrate());
			}
			
			if (BotCar.RaceLogs.Last().StartLane != BotCar.RaceLogs.Last().EndLane)
			{
				//Console.WriteLine("Switched lane, ready for next switch.");
				allowSwitch = true;
			}

			// Switch - calculate optimal lane for next curve
			if (allowSwitch && CurrentTrack.NextPiece().IsSwitch())
			{
				var lane = BotCar.FindFreeLane(OvertakingDistance);

				if (lane != TrackDirection.None)
				{
					allowSwitch = false;
					return SwitchLane(lane);
				}

				//var shortestLaneNextCurve = ShortestLane(BotCar.Position.PieceIndex, CurrentTrack.EndOfNextCurve(), BotCar.Position.InPieceDistance, 0);

				//Console.WriteLine("Current lane {0}, shortest lane {1}", BotCar.Position.Lane.EndLaneIndex, shortestLaneNextCurve);

				lane = BotCar.GetSwitchDirection(CurrentTrack.InnerLane());

				if (lane != TrackDirection.None)
				{
					allowSwitch = false;
					return SwitchLane(lane);
					
				}
			}

			if (Math.Abs(BotCar.Angle) > SlideAngle)
			{
				return Throttle(CornerThrottle);
			}

			
			if (Math.Abs(BotCar.Angle) < MaxAngle)
			{
				// Straight - full throttle
				if (CurrentTrack.CurrentPiece().IsStraight() &&
					BotCar.BrakingDistance(CurrentTrack.TargetCorneringSpeed(CurrentTrack.CurrentPiece().Index, SpeedBeforeCorner), BaseDeceleration) < CurrentTrack.DistanceToNextApex())
				{
					//if (GameState.TurboUsed < GameState.TurboAvailable && GameState.TurboAvailable + GameState.TurboLength <= GameState.GameTick)
					//{
					//	return Turbo();
					//}
					return Throttle(StraightThrottle);
				}

				//Console.WriteLine("Braking distance {0}, distance to curve {1}", BotCar.BrakingDistance(SpeedBeforeCorner, BaseDeceleration), CurrentTrack.DistanceToNextApex());

				// Slowing down for cornering
				if (BotCar.Speed() >= CurrentTrack.TargetCorneringSpeed(CurrentTrack.CurrentPiece().Index, SpeedBeforeCorner))
				{
					return Throttle(0);
				}

				return Throttle(CornerThrottle);
			}

			// Curve - vary throttle according to angle
			var throttle = GameState.LastThrottle - ThrottleDecrement;
			if (throttle < MinThrottle)
			{
				throttle = MinThrottle;
			}
			return Throttle(throttle);
		}

		private double Calibrate()
		{
			if (BaseAcceleration.IsZero())
			{
				if (GameState.GameTick < CalibrationTick || GameState.LastThrottle.IsZero())
				{
					return StraightThrottle;
				}
				BaseAcceleration = BotCar.AverageAcceleration(0, CalibrationTick);
				//BaseAcceleration = BotCar.RaceLogs[CalibrationTick].Speed - BotCar.RaceLogs[CalibrationTick - 1].Speed;
				Console.WriteLine("Base acceleration is  {0}", BaseAcceleration);
				return 0d;
			}

			if (GameState.GameTick < CalibrationTick * 2)
			{
				return 0d;
			}

			BaseDeceleration = -BotCar.AverageAcceleration(CalibrationTick, GameState.GameTick);
			//BaseDeceleration = BotCar.RaceLogs[CalibrationTick].Speed - BotCar.Speed();
			Console.WriteLine("Base deceleration is  {0}", BaseDeceleration);
			Calibrated = true;
			return StraightThrottle;
		}

		
	}
}
