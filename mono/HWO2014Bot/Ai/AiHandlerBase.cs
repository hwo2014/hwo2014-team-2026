﻿using System;
using System.Collections.Generic;
using System.Linq;
using HWO2014Bot.Messaging;
using HWO2014Bot.Model;
using HWO2014Bot.Util;

namespace HWO2014Bot.Ai
{
	public class AiHandlerBase : IAiHandler
	{
		public IMessage React(MessageWrapper message)
		{
			if (message == null)
			{
				Console.WriteLine("No message.");
				return null;
			}

			switch (message.MessageType)
			{
				case "carPositions":
					return HandleCarPositions(CarPositionsMessage.MessageFromWrapper(message));

				case "join":
					Console.WriteLine("Joined");
					return HandleJoin(JoinMessage.MessageFromData(message.Data));

				case "gameInit":
					Console.WriteLine("Race init");
					return HandleGameInit(GameInitMessage.MessageFromData(message.Data));

				case "gameEnd":
					Console.WriteLine("Race ended");
					return HandleGameEnd(GameEndMessage.MessageFromData(message.Data));

				case "gameStart":
					Console.WriteLine("Race starts");
					return HandleGameStart(GameStartMessage.MessageFromData(message.Data));

				case "yourCar":
					return HandleYourCar(YourCarMessage.MessageFromData(message.Data));

				case "tournamentEnd":
					Console.WriteLine("Tournament ended");
					return HandleTournamentEnd(TournamentEndMessage.MessageFromData(message.Data));

				case "crash":
					Console.WriteLine("Crashed!");
					return HandleCrash(CrashMessage.MessageFromData(message.Data));

				case "spawn":
					Console.WriteLine("Respawned");
					return HandleSpawn(SpawnMessage.MessageFromData(message.Data));

				case "lapFinished":
					Console.WriteLine("Lap finished");
					return HandleLapFinished(LapFinishedMessage.MessageFromData(message.Data));

				case "finish":
					Console.WriteLine("Finished");
					return HandleFinish(FinishMessage.MessageFromData(message.Data));

				case "dnf":
					Console.WriteLine("DNF");
					return HandleDnf(DnfMessage.MessageFromData(message.Data));

				case "turboAvailable":
					//Console.WriteLine("Turbo available");
					return HandleTurboAvailable(TurboAvailableMessage.MessageFromData(message.Data));

				default:
					return HandleOther(message);
			}
		}

		protected virtual IMessage HandleCarPositions(CarPositionsMessage carPositionsMessage)
		{
			SaveCarPositions(carPositionsMessage);
			
			return GameState.Crashed
				? new PingMessage()
				: CalculateMove();
		}

		protected virtual IMessage CalculateMove()
		{
			return new ThrottleMessage
			{
				Throttle = 1.0
			};
		}

		protected virtual IMessage HandleJoin(JoinMessage messageFromData)
		{
			return new PingMessage();
		}

		protected virtual IMessage HandleGameInit(GameInitMessage gameInitMessage)
		{
			SaveGameState(gameInitMessage);
			return new PingMessage();
		}

		protected virtual IMessage HandleGameEnd(GameEndMessage gameEndMessage)
		{
			return new PingMessage();
		}

		protected virtual IMessage HandleGameStart(GameStartMessage gameStartMessage)
		{
			return new PingMessage();
		}

		protected virtual IMessage HandleYourCar(YourCarMessage yourCarMessage)
		{
			SaveBotCarId(yourCarMessage);
			return new PingMessage();
		}

		protected virtual IMessage HandleTournamentEnd(TournamentEndMessage tournamentEndMessage)
		{
			return new PingMessage();
		}

		protected virtual IMessage HandleCrash(CrashMessage crashMessage)
		{
			GameState.Crashed = true;
			return new PingMessage();
		}

		protected virtual IMessage HandleSpawn(SpawnMessage spawnMessage)
		{
			GameState.Crashed = false;
			return CalculateMove();
		}

		protected virtual IMessage HandleLapFinished(LapFinishedMessage lapFinishedMessage)
		{
			Console.WriteLine("Lap {0}: {1} ticks, {2} milliseconds", lapFinishedMessage.LapTime.Laps, lapFinishedMessage.LapTime.Ticks, lapFinishedMessage.LapTime.Milliseconds);
			SaveLapTimes(lapFinishedMessage);
			return new PingMessage();
		}

		protected virtual IMessage HandleFinish(FinishMessage finishMessage)
		{
			return new PingMessage();
		}

		protected virtual IMessage HandleDnf(DnfMessage dnfMessage)
		{
			return new PingMessage();
		}

		private IMessage HandleTurboAvailable(TurboAvailableMessage messageFromData)
		{
			GameState.TurboAvailable = GameState.GameTick;
			GameState.TurboLength = messageFromData.TurboDurationTicks;
			GameState.TurboFactor = messageFromData.TurboFactor;
			return CalculateMove();
		}

		protected virtual IMessage HandleOther(MessageWrapper message)
		{
			return new PingMessage();
		}

		protected virtual ThrottleMessage Throttle(double throttle)
		{
			//Console.WriteLine("Throttle {0}", throttle);
			GameState.LastThrottle = throttle;
			return new ThrottleMessage { Throttle = throttle };
		}

		protected virtual SwitchLaneMessage SwitchLane(TrackDirection direction)
		{
			//Console.WriteLine("Switch lane to {0}, current lane {1}", direction, GameState.BotCar().Position.Lane.EndLaneIndex);
			GameState.LastLaneSwitch = direction;
			return new SwitchLaneMessage {Direction = direction};
		}

		protected virtual TurboMessage Turbo()
		{
			//Console.WriteLine("Using turbo");
			GameState.TurboUsed = GameState.GameTick;
			return new TurboMessage();
		}

		private void SaveCarPositions(CarPositionsMessage carPositionsMessage)
		{
			GameState.GameTick = carPositionsMessage.GameTick;
			GameState.GameId = carPositionsMessage.GameId;

			foreach (var car in carPositionsMessage.Cars)
			{
				if (GameState.Cars.ContainsKey(car.Id))
				{
					GameState.Cars[car.Id].Position = car.Position;
					GameState.Cars[car.Id].Angle = car.Angle;
					GameState.Cars[car.Id].Lap = car.Lap;
				}
				else
				{
					GameState.Cars.Add(car.Id, car);
				}

				var savedCar = GameState.Cars[car.Id];

				if (savedCar.RaceLogs == null)
				{
					savedCar.RaceLogs = new List<RaceLog>();
				}

				savedCar.RaceLogs.Add(new RaceLog
				{
					Tick = carPositionsMessage.GameTick,
					Lap = savedCar.Lap,
					TrackPiece = savedCar.Position.PieceIndex,
					InPieceDistance = savedCar.Position.InPieceDistance,
					Angle = savedCar.Angle,
					StartLane = savedCar.Position.Lane.StartLaneIndex,
					EndLane = savedCar.Position.Lane.EndLaneIndex,
					Throttle = GameState.LastThrottle,
					Speed = savedCar.Speed(),
					Car = savedCar
				});
			}
		}

		private void SaveGameState(GameInitMessage gameInitMessage)
		{
			if (GameState.Tracks == null)
			{
				GameState.Tracks = new Dictionary<string, Track>();
			}
			var track = gameInitMessage.Race.Track;
			if (GameState.Tracks.ContainsKey(track.Id))
			{
				GameState.Tracks[track.Id] = track;
			}
			else
			{
				GameState.Tracks.Add(track.Id, track);
			}

			Console.WriteLine("Track: {0}", track.Id);

			foreach (var trackPiece in track.Pieces)
			{
				trackPiece.Track = track;
				trackPiece.Index = track.Pieces.IndexOf(trackPiece);
				Console.WriteLine("{0};{1};{2};{3};{4};{5}", trackPiece.Index, trackPiece.Length, trackPiece.Angle, trackPiece.Radius, trackPiece.Switch, trackPiece.PieceLength(0));
			}

			Console.WriteLine("Lanes:");
			foreach (var lane in track.Lanes)
			{
				Console.WriteLine("{0};{1}", lane.Index,lane.DistanceFromCenter);
			}
			
			GameState.CurrentTrackId = track.Id;
			GameState.Cars = gameInitMessage.Race.Cars.ToDictionary(k => k.Id, v => v);
		}

		private void SaveLapTimes(LapFinishedMessage lapFinishedMessage)
		{
			if (GameState.Cars[lapFinishedMessage.CarId].LapTimes == null)
			{
				GameState.Cars[lapFinishedMessage.CarId].LapTimes = new List<LapTime>();
			}

			GameState.Cars[lapFinishedMessage.CarId].LapTimes.Add(new LapTime
			{
				Lap = lapFinishedMessage.LapTime.Laps,
				Ticks = lapFinishedMessage.LapTime.Ticks,
				Milliseconds = lapFinishedMessage.LapTime.Milliseconds
			});

			GameState.CurrentLap = lapFinishedMessage.LapTime.Laps;
		}

		private void SaveBotCarId(YourCarMessage yourCarMessage)
		{
			GameState.BotCarId = new CarId
			{
				Name = yourCarMessage.Name,
				Color = yourCarMessage.Color
			};
		}
	}
}
