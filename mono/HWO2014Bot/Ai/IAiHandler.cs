﻿using HWO2014Bot.Messaging;

namespace HWO2014Bot.Ai
{
	public interface IAiHandler
	{
		IMessage React(MessageWrapper message);
	}
}
