﻿using System;
using System.IO;
using System.Net.Sockets;
using HWO2014Bot.Ai;
using HWO2014Bot.Messaging;

namespace HWO2014Bot
{
	public class BotProgram
	{
		public static void Main(string[] args)
		{
			var host = args[0];
			var port = int.Parse(args[1]);
			var botName = args[2];
			var botKey = args[3];

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			using (var client = new TcpClient(host, port))
			{
				var stream = client.GetStream();
				var reader = new StreamReader(stream);
				var writer = new StreamWriter(stream) { AutoFlush = true };

				new BotProgram(reader, writer, botName, botKey);
			}
		}

		public BotProgram(StreamReader reader, StreamWriter writer, string botName, string botKey)
		{
			var messageClient = new MessageClient
			{
				Reader = reader,
				Writer = writer
			};

			var ai = new MarkTwoAi();
			
			IMessage message = new JoinMessage
			{
				Name = botName,
				Key = botKey
			};

			while (message != null)
			{
				var response = messageClient.Send(message);
				message = ai.React(response);
			}

			Console.WriteLine("Exiting bot program.");
		}
	}
}
