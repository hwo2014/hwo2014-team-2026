﻿using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class TrackPiece
	{
		[JsonProperty(PropertyName = "length")]
		public double Length { get; set; }

		[JsonProperty(PropertyName = "radius")]
		public int Radius { get; set; }

		[JsonProperty(PropertyName = "angle")]
		public double Angle { get; set; }

		[JsonProperty(PropertyName = "switch")]
		public bool Switch { get; set; }

		[JsonIgnore]
		public Track Track { get; set; }

		[JsonIgnore]
		public int Index { get; set; }
	}
}
