﻿namespace HWO2014Bot.Model
{
	public enum TrackDirection
	{
		None = 0,
		Left = 1,
		Right = 2
	}
}