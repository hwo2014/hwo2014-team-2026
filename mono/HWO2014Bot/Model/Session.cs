﻿using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class Session
	{
		[JsonProperty(PropertyName = "laps")]
		public int Laps { get; set; }

		[JsonProperty(PropertyName = "maxLapTimeMs")]
		public int MaxLapTimeMs { get; set; }

		[JsonProperty(PropertyName = "quickRace")]
		public bool QuickRace { get; set; }
	}
}
