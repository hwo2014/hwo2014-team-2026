﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class Race
	{
		[JsonProperty(PropertyName = "track")]
		public Track Track { get; set; }

		[JsonProperty(PropertyName = "cars")]
		public IEnumerable<Car> Cars { get; set; }

		[JsonProperty(PropertyName = "raceSession")]
		public Session RaceSession { get; set; }
	}
}
