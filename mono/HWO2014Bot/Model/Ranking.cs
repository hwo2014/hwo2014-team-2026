﻿using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class Ranking
	{
		[JsonProperty(PropertyName = "overall")]
		public int Overall { get; set; }

		[JsonProperty(PropertyName = "fastestLap")]
		public int FastestLap { get; set; }
	}
}
