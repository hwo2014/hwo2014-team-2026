﻿
namespace HWO2014Bot.Model
{
	public class LapTime
	{
		public int Lap { get; set; }

		public int Ticks { get; set; }

		public int Milliseconds { get; set; }
	}
}
