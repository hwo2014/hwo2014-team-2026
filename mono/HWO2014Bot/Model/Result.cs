﻿using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class Result
	{
		[JsonProperty(PropertyName = "car")]
		public CarId Car { get; set; }

		[JsonProperty(PropertyName = "result")]
		public Timing Timing { get; set; }
	}

	public class Timing
	{
		[JsonProperty(PropertyName = "laps")]
		public int Laps { get; set; }

		[JsonProperty(PropertyName = "ticks")]
		public int Ticks { get; set; }

		[JsonProperty(PropertyName = "millis")]
		public int Milliseconds { get; set; }
	}
}
