﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class Track
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "pieces")]
		public List<TrackPiece> Pieces { get; set; }

		[JsonProperty(PropertyName = "lanes")]
		public List<Lane> Lanes { get; set; }

		[JsonProperty(PropertyName = "startingPoint")]
		public StartingPoint StartingPoint { get; set; }
	}

	public class StartingPoint
	{
		[JsonProperty(PropertyName = "position")]
		public Position Position { get; set; }

		[JsonProperty(PropertyName = "angle")]
		public double Angle { get; set; }
	}

	public class Position
	{
		[JsonProperty(PropertyName = "x")]
		public double X { get; set; }

		[JsonProperty(PropertyName = "y")]
		public double Y { get; set; }
	}
}
