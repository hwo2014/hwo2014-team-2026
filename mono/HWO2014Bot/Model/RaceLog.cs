﻿
namespace HWO2014Bot.Model
{
	public class RaceLog
	{
		public int Tick { get; set; }

		public int Lap { get; set; }

		public int TrackPiece { get; set; }

		public double InPieceDistance { get; set; }

		public double Angle { get; set; }

		public int StartLane { get; set; }

		public int EndLane { get; set; }

		public double Throttle { get; set; }

		public double Speed { get; set; }

		public double Acceleration { get; set; }

		public Car Car { get; set; }
	}
}
