﻿using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class BotId
	{
		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "key")]
		public string Key { get; set; }
	}
}
