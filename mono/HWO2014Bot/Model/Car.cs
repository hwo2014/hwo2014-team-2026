﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class Car
	{
		[JsonProperty(PropertyName = "id")]
		public CarId Id { get; set; }

		[JsonProperty(PropertyName = "dimensions")]
		public CarDimensions Dimensions { get; set; }

		[JsonProperty(PropertyName = "angle")]
		public double Angle { get; set; }

		[JsonProperty(PropertyName = "piecePosition")]
		public PiecePosition Position { get; set; }

		[JsonProperty(PropertyName = "lap")]
		public int Lap { get; set; }

		[JsonIgnore]
		public List<RaceLog> RaceLogs { get; set; }

		[JsonIgnore]
		public List<LapTime> LapTimes { get; set; }
	}

	public class CarId
	{
		protected bool Equals(CarId other)
		{
			return string.Equals(Name, other.Name) && string.Equals(Color, other.Color);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Name != null ? Name.GetHashCode() : 0)*397) ^ (Color != null ? Color.GetHashCode() : 0);
			}
		}

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "color")]
		public string Color { get; set; }

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((CarId) obj);
		}
	}

	public class CarDimensions
	{
		[JsonProperty(PropertyName = "length")]
		public double Length { get; set; }

		[JsonProperty(PropertyName = "width")]
		public double Width { get; set; }

		[JsonProperty(PropertyName = "guideFlagPosition")]
		public double GuideFlagPosition { get; set; }
	}

	public class PiecePosition
	{
		[JsonProperty(PropertyName = "pieceIndex")]
		public int PieceIndex { get; set; }

		[JsonProperty(PropertyName = "inPieceDistance")]
		public double InPieceDistance { get; set; }

		[JsonProperty(PropertyName = "lane")]
		public InPieceLane Lane { get; set; }
	}

	public class InPieceLane
	{
		[JsonProperty(PropertyName = "startLaneIndex")]
		public int StartLaneIndex { get; set; }

		[JsonProperty(PropertyName = "endLaneIndex")]
		public int EndLaneIndex { get; set; }
	}
}
