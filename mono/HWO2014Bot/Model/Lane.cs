﻿using Newtonsoft.Json;

namespace HWO2014Bot.Model
{
	public class Lane
	{
		[JsonProperty(PropertyName = "distanceFromCenter")]
		public int DistanceFromCenter { get; set; }

		[JsonProperty(PropertyName = "index")]
		public int Index { get; set; }
	}
}
